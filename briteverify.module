<?php

/**
 * BriteVerify service integration
 * @author Raman Liubimau <raman@cmstuning.net>
 */


/**
 * Implements of Drupal's hook_menu().
 * Add configuration page.
 * @return array
 */
function briteverify_menu() {
  $items['admin/config/people/briteverify-email-validation'] = array(
    'title' => ' BriteVerify email validation',
    'description' => 'Configure BriteVerify service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('briteverify_config_form'),
    'access arguments' => array('administer permissions'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements of Drupal's hook_form().
 * Get BriteVerify configuration settings.
 * @param array $form
 * @param array $form_state
 * @return array
 */
function briteverify_config_form($form, &$form_state) {
  $form['api-key'] = array(
    '#type' => 'textfield',
    '#title' => t('<b>API</b> key'),
    '#description' => t('You can find your API key in the BriteVerify account.'),
    '#required' => TRUE,
    '#default_value' => variable_get('briteverify_api_key', ''),
  );
  $form['error'] = array(
    '#type' => 'textfield',
    '#title' => t('Error message to display'),
    '#description' => t('You can specify your own error message to display when email check is failed.'),
    '#required' => TRUE,
    '#default_value' => variable_get('briteverify_error', 'BriteVerify cannot check this email. Are you sure that it is valid?'),
  );
  $form['accept'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Also consider email\'s with the following statuses correct: '),
    '#description' => t('By default only emails with strong valid status are accepted.'),
    '#options' => array(
      'unknown' => t('<b>Unknown</b>: for some reson we cannot verify valid or invalid.'),
      'accept_all' => t('<b>Accept All</b>: email cannot be fully verified.')),
    '#default_value' => variable_get('briteverify_accept', array()),
  );
  $form['ignore'] = array(
    '#type' => 'textarea',
    '#title' => t('Disable BriteVerify for the following domains or emails:'),
    '#description' => t('Please, use one domain or email per line.'),
    '#default_value' => variable_get('briteverify_ignore', ''),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Implements of Drupal's hook_form_submit().
 * Update BriteVerify configuration settings.
 * @param array $form
 * @param array $form_state
 */
function briteverify_config_form_submit($form, &$form_state) {
  $api_key = $form_state['values']['api-key'];
  $ignore = $form_state['values']['ignore'];
  $accept = $form_state['values']['accept'];
  $error = $form_state['values']['error'];

  variable_set('briteverify_api_key', $api_key);
  variable_set('briteverify_ignore', $ignore);
  variable_set('briteverify_error', $error);
  variable_set('briteverify_accept', $accept);
}

/**
 * Check email using BriteVerify service.
 * Note that all call's are billable.
 * @param string $email
 * @return bool
 */
function briteverify_email_check($email) {
  $url = url('https://bpi.briteverify.com/emails.json', array('query' => array(
    'address' => $email,
    'apikey' => variable_get('briteverify_api_key', ''),
  )));
  
  $response = drupal_http_request($url);
  
  if (isset($response->error)) {
    drupal_set_message(t('BriteVerify request error'), 'error');
    return FALSE;
  }
  
  $info = drupal_json_decode($response->data);
  $status = $info['status'];
  
  $default = array(
    'valid' => TRUE,
    'invalid' => FALSE,
  );
  
  $accept = array_merge($default, variable_get('briteverify_accept', array()));
  
  return (isset($accept[$status]) && (bool)$accept[$status]);
}

/**
 * Implements of Drupal's hook_form_alter().
 * Add custom validate function.
 * @param array $form
 * @param array $form_state
 * @param string $form_id
 */
function briteverify_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id === 'user_register_form') {
    $form['#validate'][] = 'briteverify_register_form_validate';
  }
}

/**
 * Implements of Drupal's hook_form_validate().
 * Check user email using BriteVerify service.
 * @param array $form
 * @param array $form_state
 */
function briteverify_register_form_validate($form, &$form_state) {  
  $email = $form_state['values']['mail'];  
  
  // Mix of ignored emails and domains
  $ignore = explode("\n", variable_get('briteverify_ignore', ''));
  
  foreach ($ignore as $token) {
    // Skip check if email is ignored
    if (preg_match('/.*@.*/', $token)) {
      // Skip check if email is ignored
      if ($email === $token) {
        return;
      }
      continue;
    }
    
    if (preg_match('/.*@('.trim($token).')$/', $email)) {
      watchdog('notice', 'check skipped domain.');
      return;
    }
  }
  
  if (!briteverify_email_check($email)) {
    form_set_error('mail', t(variable_get('briteverify_error')));
  }
}